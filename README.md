# Mediawiki, OBSD Notes
*State: Initial Draft*

Todo:
- Enable TLS on nginx.conf;

- Installation details;

## Pre-Setup
1. Server Setup
*In case the instance was provisioned without the xbase set installed, it might not be possible to install some additional softwares like ImageMagick or php-gd due to missing libs. It's possible to install them afterwards.*
Installing the missing xbase sets:
```
$ doas curl -LO 'https://ftp.usa.openbsd.org/pub/OpenBSD/7.0/amd64/x{base,serv,font,share}70.tgz'
$ for file in xbase70 xfont70 xserv70 xshare70; do doas tar -xvzf $file.tgz -C /; done
```
2. Install the necessary packages
```
$ doas pkg_add postgresql-server nginx php%7.4 php-pgsql%7.4 php-pdo_pgsql%7.4 php-intl%7.4.26 php-gd%7.4 pecl74-imagick
quirks-4.53: ok
postgresql-server-13.4p0:xz-5.2.5: ok
postgresql-server-13.4p0:libiconv-1.16p0: ok
postgresql-server-13.4p0:libxml-2.9.12: ok
postgresql-server-13.4p0:postgresql-client-13.4p0: ok
postgresql-server-13.4p0: ok
nginx-1.20.1p1:pcre-8.44: ok
nginx-1.20.1p1: ok
php-7.4.26:gettext-runtime-0.21p1: ok
php-7.4.26:bzip2-1.0.8p0: ok
php-7.4.26:pcre2-10.36: ok
php-7.4.26:oniguruma-6.9.7.1: ok
php-7.4.26:femail-1.0p1: ok
php-7.4.26:femail-chroot-1.0p3: ok
php-7.4.26:argon2-20190702: ok
php-7.4.26:libsodium-1.0.18p1: ok
php-7.4.26: ok
php-pgsql-7.4.26: ok
php-intl-7.4.26:icu4c-69.1p0v0: ok
php-intl-7.4.26:icu4c-wwwdata-69.1p0v0: ok
php-intl-7.4.26: ok
Running tags: ok
The following new rcscripts were installed: /etc/rc.d/nginx /etc/rc.d/php74_fpm /etc/rc.d/postgresql
See rcctl(8) for details.
New and changed readme(s):
	/usr/local/share/doc/pkg-readmes/femail-chroot
	/usr/local/share/doc/pkg-readmes/nginx
	/usr/local/share/doc/pkg-readmes/php-7.4
	/usr/local/share/doc/pkg-readmes/postgresql-server
```
3. PKI Setup for TLS support (Optional)
Setting up the root CA and Private Key
```
$ doas mkdir -p /usr/local/ssl/private
$ doas openssl req -days 3650 -nodes -new -x509 -keyout /usr/local/ssl/private/ca.key -out /usr/local/ssl/ca.crt
Generating a 2048 bit RSA private key
........+++++
..........................................+++++
writing new private key to '/usr/local/ssl/private/ca.key'
-----
You are about to be asked to enter information that will be incorporated
into your certificate request.
What you are about to enter is what is called a Distinguished Name or a DN.
There are quite a few fields but you can leave some blank
For some fields there will be a default value,
If you enter '.', the field will be left blank.
-----
Country Name (2 letter code) []:EX
State or Province Name (full name) []:Example
Locality Name (eg, city) []:Example
Organization Name (eg, company) []:Example IT
Organizational Unit Name (eg, section) []:Technology
Common Name (eg, fully qualified host name) []:ca.example.net
Email Address []:
```
Setting up the Private Key and the CSR
```
$ doas openssl req -days 3650 -nodes -new -keyout /usr/local/ssl/private/server.key -out /usr/local/ssl/private/server.csr
Generating a 2048 bit RSA private key
...............+++++
......................................................................................................................................................+++++
writing new private key to '/usr/local/ssl/private/server.key'
-----
You are about to be asked to enter information that will be incorporated
into your certificate request.
What you are about to enter is what is called a Distinguished Name or a DN.
There are quite a few fields but you can leave some blank
For some fields there will be a default value,
If you enter '.', the field will be left blank.
-----
Country Name (2 letter code) []:EX
State or Province Name (full name) []:Example
Locality Name (eg, city) []:Example
Organization Name (eg, company) []:Example IT
Organizational Unit Name (eg, section) []:Technology
Common Name (eg, fully qualified host name) []:*.example.net
Email Address []:

Please enter the following 'extra' attributes
to be sent with your certificate request
A challenge password []:
```
Signing the CSR and generating the CRT
```
$ doas openssl -x509 -req -days 3650 -in /usr/local/ssl/private/server.csr -out /usr/local/ssl/cert.crt -CA /usr/local/ssl/ca.crt -CAkey /usr/local/ssl/private/ca.key -CAcreateserial
Signature ok
subject=/C=EX/ST=Example/L=Example/O=Example IT/OU=Technology/CN=*.example.net
Getting CA Private Key

```
## Database Setup
1. Initiate the Database
```
$ doas su - _postgresql
$ initdb -D /var/postgresql/data -U postgres --auth=scram-sha-256 --pwprompt --encoding=UTF-8
The files belonging to this database system will be owned by user "_postgresql".
This user must also own the server process.

The database cluster will be initialized with locale "C".
The default text search configuration will be set to "english".

Data page checksums are disabled.

Enter new superuser password: 
Enter it again: 

creating directory /var/postgresql/data ... ok
creating subdirectories ... ok
selecting dynamic shared memory implementation ... posix
selecting default max_connections ... 20
selecting default shared_buffers ... 128MB
selecting default time zone ... UTC
creating configuration files ... ok
running bootstrap script ... ok
performing post-bootstrap initialization ... ok
syncing data to disk ... ok

Success. You can now start the database server using:

    pg_ctl -D /var/postgresql/data -l logfile start

```
2. Enable and Start Postgres
```
$ doas rcctl enable postgresql && doas rcctl start postgresql
```
3. User & Database Setup
```
$ createuser -SDRPE wikiadmin -U postgres
$ createdb -O wikiadmin wikidb
```
## Mediawiki/Web Setup
1. Enable the PHP extensions
```
doas ln -sf /etc/php-7.4.sample/* /etc/php-7.4/
```
2. Configure NGINX
*see attached file*
```
$ doas /etc/nginx/nginx.conf
```
3. Setup Mediawiki files
```
$ curl -O https://releases.wikimedia.org/mediawiki/1.37/mediawiki-1.37.0.tar.gz

% Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100 48.4M  100 48.4M    0     0  2112k      0  0:00:23  0:00:23 --:--:-- 4105k

$ doas tar -xvzf mediawiki-1.37.0.tar.gz -C /var/www/
$ doas mv /var/www/mediawiki-1.37.0 /var/www/mediawiki
```
4. Enable and Start Services
```
$ for svc in php74_fpm nginx; do doas rcctl enable $svc && doas rcctl start $svc; done
php74_fpm(ok)
nginx(ok)
```
5. Proceed with Installation
